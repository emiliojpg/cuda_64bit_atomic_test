TARGET=test

NVCC=nvcc
CXXFLAGS=-O3 -Xcompiler -Wall -Xcompiler -Wextra

$(TARGET): $(TARGET).cu
	$(NVCC) $(CXXFLAGS) $^ -o $@

run: $(TARGET)
	for i in 500 5000 50000 500000 5000000 50000000 500000000; do ./$(TARGET) $$i; done

clean:
	$(RM) -f *~ *.o

cleanall:
	make clean

	$(RM) $(TARGET)
