//
// Simple code to test the performance of a 64-bit Min atomic in CUDA
//
// Derived from this example in stackoverflow by Robert Crovella
// https://stackoverflow.com/questions/17411493/how-can-i-implement-a-custom-atomic-function-involving-several-variables
//

#include <cstdio>
#include <chrono>
#include <iostream>
#include <iomanip>

#define DSIZE 5000000
#define nTPB 256

#define cudaCheckErrors(msg)                                    \
  do {                                                          \
    cudaError_t __err = cudaGetLastError();                     \
    if (__err != cudaSuccess) {                                 \
      fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n",        \
              msg, cudaGetErrorString(__err),                   \
              __FILE__, __LINE__);                              \
      fprintf(stderr, "*** FAILED - ABORTING\n");               \
      exit(1);                                                  \
    }                                                           \
  } while (0)

typedef union {
  struct {
    unsigned int pointid;
    float dist;
  };
  unsigned long long int dist64;
} frag;

__device__ frag test;

__global__ void min_test(const float* data, const size_t nfrags)
{
  int idx = (blockDim.x * blockIdx.x) + threadIdx.x;
  if (idx < nfrags) {
    frag thisfrag;
    thisfrag.dist = data[idx];
    thisfrag.pointid = idx;
    atomicMin(&(test.dist64), thisfrag.dist64);
  }
}

__device__ unsigned long long int my_atomicMin(unsigned long long int* address, float val1, int val2)
{
  frag loc, loctest;
  loc.dist = val1;
  loc.pointid = val2;
  loctest.dist64 = *address;
  while (loctest.dist > val1)
    loctest.dist64 = atomicCAS(address, loctest.dist64, loc.dist64);

  return loctest.dist64;
}

__global__ void min_test2(const float* data, const size_t nfrags)
{
  int idx = (blockDim.x * blockIdx.x) + threadIdx.x;
  if (idx < nfrags)
    my_atomicMin(&(test.dist64), data[idx], idx);
}

int main(int argc, char *argv[])
{
  float *d_data, *h_data;
  frag my_init;
  size_t nfrags = DSIZE;

  if (argc > 1)
    nfrags = atoi(argv[1]);

  std::cout << std::fixed << std::setprecision(12);
  std::cout << std::endl << "-> " << nfrags << " Elems" << std::endl;

  h_data = (float *) malloc(nfrags * sizeof(float));
  if (h_data == 0) {printf("malloc fail\n"); return 1;}

  cudaMalloc((void **)&d_data, nfrags * sizeof(float));
  cudaCheckErrors("cm1 fail");

  // create random floats between 0 and 1
  for (size_t i = 0; i < nfrags; i++) h_data[i] = rand()/(float)RAND_MAX;

  cudaMemcpy(d_data, h_data, nfrags*sizeof(float), cudaMemcpyHostToDevice);
  cudaCheckErrors("cmcp1 fail");


// execute with GPU my_atomicMin kernel
  my_init.dist = 10.0f;
  my_init.pointid = nfrags;
  cudaMemcpyToSymbol(test, &(my_init.dist64), sizeof(unsigned long long int));
  cudaCheckErrors("cmcp2 fail");

  auto start = std::chrono::steady_clock::now();
  min_test2<<<(nfrags+nTPB-1)/nTPB, nTPB>>>(d_data, nfrags);
  cudaDeviceSynchronize();
  auto end = std::chrono::steady_clock::now();
  cudaCheckErrors("kernel1 fail");

  cudaMemcpyFromSymbol(&(my_init.dist64), test, sizeof(unsigned long long int));
  cudaCheckErrors("cmcp3 fail");

  std::cout << std::endl << "GPU - my_atomicMin" << std::endl;
  std::cout << "device min result = " << my_init.dist << std::endl;
  std::cout << "device idx result = " << my_init.pointid << std::endl;
  std::cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " us" << std::endl;


  // execute with GPU atomicMin kernel
  my_init.dist = 10.0f;
  my_init.pointid = nfrags;
  cudaMemcpyToSymbol(test, &(my_init.dist64), sizeof(unsigned long long int));
  cudaCheckErrors("cmcp4 fail");

  start = std::chrono::steady_clock::now();
  min_test<<<(nfrags+nTPB-1)/nTPB, nTPB>>>(d_data, nfrags);
  cudaDeviceSynchronize();
  end = std::chrono::steady_clock::now();
  cudaCheckErrors("kernel2 fail");

  cudaMemcpyFromSymbol(&(my_init.dist64), test, sizeof(unsigned long long int));
  cudaCheckErrors("cmcp5 fail");

  std::cout << std::endl << "GPU - atomicMin" << std::endl;
  std::cout << "device min result = " << my_init.dist << std::endl;
  std::cout << "device idx result = " << my_init.pointid << std::endl;
  std::cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " us" << std::endl;


  // execute with CPU kernel
  float host_val = 10.0f;
  unsigned int host_idx = nfrags;
  start = std::chrono::steady_clock::now();
  for (size_t i=0; i<nfrags; i++)
    if (h_data[i] < host_val){
      host_val = h_data[i];
      host_idx = i;
    }
  end = std::chrono::steady_clock::now();

  std::cout << std::endl << "CPU" << std::endl;
  std::cout << "host min result = " << host_val << std::endl;
  std::cout << "host idx result = " << host_idx << std::endl;
  std::cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " us" << std::endl;

  std::cout << std::endl;

  return 0;
}
